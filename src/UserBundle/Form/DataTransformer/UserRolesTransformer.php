<?php

namespace UserBundle\Form\DataTransformer;

use Alpha\A;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\DataTransformerInterface;
use UserBundle\Entity\Role;

/**
 * Class UserRolesTransformer
 * @package UserBundle\Form\DataTransformer
 */
class UserRolesTransformer implements DataTransformerInterface
{
    /**
     * @var ObjectManager
     */
    private $manager;

    /**
     * RolesArrayToCollectionTransformer constructor.
     */
    public function __construct(ObjectManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * Array vers ArrayCollection
     * @param mixed $value
     * @return ArrayCollection
     */
    public function transform($value)
    {
        $rolesCollection = new ArrayCollection();
        foreach ($value as $roleString) {
            $rolesCollection->add($this->manager->getRepository('UserBundle:Role')->findOneByRole($roleString));
        }
        return $rolesCollection;
    }

    /**
     * ArrayCollection vers Array
     * @param mixed $arrayCollection
     * @return mixed
     */
    public function reverseTransform($arrayCollection)
    {
        return $arrayCollection->toArray();
    }

}