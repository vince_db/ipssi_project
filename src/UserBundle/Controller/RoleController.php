<?php

namespace UserBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use UserBundle\Entity\Role;
use UserBundle\Form\RoleType;

/**
 * Role controller.
 *
 * @Route("user/role")
 *
 * @todo Construire les vues pour admin des roles
 */
class RoleController extends Controller
{
    /**
     * Lists all Role entities.
     *
     * @Route("/", name="role_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $roles = $em->getRepository('UserBundle:Role')->findAll();

        return $this->render('user/role/index.html.twig', array(
            'zone' => 'user',
            'roles' => $roles,
            'title' => 'Rôles : Liste des rôles',
        ));
    }

    /**
     * Finds and displays a Role entity.
     *
     * @Route("/{id}", name="role_show")
     * @Method("GET")
     */
    public function showAction(Role $role)
    {
        return $this->render('user/role/show.html.twig', array(
            'zone' => 'user',
            'role' => $role,
            'title' => 'Rôle : voir rôle ' . $role->getName(),
        ));
    }

    /**
     * Displays a form to edit an existing Role entity.
     * @todo : empcher l'édition de $role->role, limiter à $role->name
     *
     * @Route("/{id}/edit", name="role_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Role $role)
    {
        $editForm = $this->createForm('UserBundle\Form\RoleType', $role);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($role);
            $em->flush();

            return $this->redirectToRoute('role_edit', array('id' => $role->getId()));
        }

        return $this->render('user/role/edit.html.twig', array(
            'role' => $role,
            'edit_form' => $editForm->createView(),
            'title' => 'Rôles : Éditer un rôle',
        ));
    }

}
