<?php

namespace CMSBundle\Form;

use CMSBundle\Form\DataTransformer\FileTransformer;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ArticleType extends AbstractType
{
    /**
     * @var ObjectManager
     */
    private $manager;

    /**
     * @var
     */
    private $uploadDir;

    /**
     * ArticleType constructor.
     */
    public function __construct(ObjectManager $manager, $uploadDir)
    {
        $this->manager = $manager;
        $this->uploadDir = $uploadDir;
    }


    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('banner', FileType::class, array('label' => 'Banniere', 'required' => false))
            ->add('teaser')
            ->add('body')
            ->add('published')
            ->add('tracked')
            ->add('public')
            ->add('category')
        ;
        $builder->get('banner')->addModelTransformer(new FileTransformer($this->manager, $this->uploadDir));
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CMSBundle\Entity\Article'
        ));
        $resolver->setRequired('entity_manager');
    }
}
