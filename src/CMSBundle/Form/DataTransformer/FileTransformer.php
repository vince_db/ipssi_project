<?php

namespace CMSBundle\Form\DataTransformer;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\HttpFoundation\File\File;

/**
 * Class FileTransformer
 * @package CMSBundle\Form\DataTransformer
 */
class FileTransformer implements DataTransformerInterface
{
    /**
     * @var ObjectManager
     */
    private $manager;

    /**
     * @var
     */
    private $uploadDir;

    /**
     * SkillsTransformer constructor.
     */
    public function __construct(ObjectManager $manager, $uploadDir)
    {
        $this->manager = $manager; // @todo-vince : retirer car inutile ici
        $this->uploadDir = $uploadDir;
    }

    /**
     * @param mixed $value
     * @return mixed
     */
    public function transform($value)
    {
        if(!is_null($value)) {
            $value = new File($this->uploadDir . '/' . $value);
        }
        return $value;
    }

    /**
     * @param mixed $value
     * @return string
     */
    public function reverseTransform($value)
    {
        return $value;
    }

}