<?php

namespace CMSBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use CMSBundle\Entity\Article;
use CMSBundle\Form\ArticleType;
use Symfony\Component\HttpFoundation\File\File as File;

/**
 * Article controller.
 *
 * @Route("/cms/article")
 */
class ArticleController extends Controller
{
    /**
     * Lists all Article entities.
     *
     * @Route("/", name="article_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $articles = $em->getRepository('CMSBundle:Article')->findAll();

        return $this->render('cms/article/index.html.twig', array(
            'zone' => 'admin',
            'articles' => $articles,
            'title' => 'CMS : Articles : Liste des articles'
        ));
    }
    /**
     * Creates a new Article entity.
     *
     * @Route("/new", name="article_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $article = new Article();
        $form = $this->createForm('CMSBundle\Form\ArticleType', $article);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->container->get('article.service')->save($article);
            return $this->redirectToRoute('article_show', array('id' => $article->getId()));
        }

        return $this->render('cms/article/new.html.twig', array(
            'zone' => 'admin',
            'article' => $article,
            'form' => $form->createView(),
            'title' => 'CMS : Articles : Créer un nouvel article'
        ));
    }

    /**
     * Finds and displays a Article entity.
     *
     * @Route("/{id}", name="article_show")
     * @Method("GET")
     */
    public function showAction(Article $article)
    {
        $deleteForm = $this->createDeleteForm($article);

        return $this->render('cms/article/show.html.twig', array(
            'zone'=>'admin',
            'article' => $article,
            'delete_form' => $deleteForm->createView(),
            'title' => 'CMS : Articles : Article ' . $article->getTitle()
        ));
    }

    /**
     * Displays a form to edit an existing Article entity.
     *
     * @Route("/{id}/edit", name="article_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Article $article)
    {
            $deleteForm = $this->createDeleteForm($article);
            $editForm = $this->createForm('CMSBundle\Form\ArticleType', $article, [
               'entity_manager' => $this->get('doctrine.orm.entity_manager')
            ]);
            $editForm->handleRequest($request);

            if ($editForm->isSubmitted() && $editForm->isValid()) {
               $this->container->get('article.service')->save($article);
               return $this->redirectToRoute('article_edit', array('id' => $article->getId()));
            }

            return $this->render('cms/article/edit.html.twig', array(
               'zone' => 'admin',
               'article' => $article,
               'edit_form' => $editForm->createView(),
               'delete_form' => $deleteForm->createView(),
               'title' => 'CMS : Articles : Édition de l\'article ' . $article->getTitle()
            ));
       }

   /**
    * Deletes a Article entity.
    *
    * @Route("/{id}", name="article_delete")
    * @Method("DELETE")
    */
    public function deleteAction(Request $request, Article $article)
    {
        $form = $this->createDeleteForm($article);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            unlink($this->getParameter('banners_directory') . '/' . $article->getBanner());
            $em->remove($article);
            $em->flush();
        }

        return $this->redirectToRoute('article_index');
    }

    /**
     * Creates a form to delete a Article entity.
     *
     * @param Article $article The Article entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Article $article)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('article_delete', array('id' => $article->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
