<?php

namespace CMSBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @Route("/cms")
 */
class DefaultController extends Controller
{
    /**
     * @Route("/", name="cms_default_index")
     */
    public function indexAction()
    {
        return $this->render('cms/index.html.twig');
    }
}
