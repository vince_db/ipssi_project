<?php

namespace CMSBundle\Service;

use CMSBundle\Entity\Article;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class ArticleService
 */
class ArticleService
{
    /**
     * @var ObjectManager
     */
    private $manager;
    private $bannersDirectory;

    /**
     * ArticleService constructor.
     */
    public function __construct(ObjectManager $manager, $bannersDirectory)
    {
        $this->manager = $manager;
        $this->bannersDirectory = $bannersDirectory;
    }

    /**
     * Persist article
     * @param Article $article
     */
    public function save(Article $article)
    {
        $file = $article->getBanner();
        $fileName = md5(uniqid() . '.' . $file->guessExtension());
        $file->move($this->bannersDirectory, $fileName);
        $article->setBanner($fileName);

        $this->manager->persist($article);
        $this->manager->flush();
    }

}