<?php

namespace HRManagementBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class HRManagementBundle
 * @package HRManagementBundle
 *
 * @todo Revoir les routes et l'arborescense des fichiers vues
 */
class HRManagementBundle extends Bundle
{
}
