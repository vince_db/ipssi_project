<?php

namespace HRManagementBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use HRManagementBundle\Entity\Expense;
use HRManagementBundle\Form\ExpenseType;

/**
 * Expense controller.
 *
 * @Route("/hr/management/expense")
 */
class ExpenseController extends Controller
{
    /**
     * Lists all Expense entities.
     *
     * @Route("/", name="expense_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $expenses = $em->getRepository('HRManagementBundle:Expense')->findAll();

        return $this->render('hr/management/expense/index.html.twig', array(
            'zone'=>'hr',
            'expenses' => $expenses,
        ));
    }

    /**
     * Creates a new Expense entity.
     *
     * @Route("/new", name="expense_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $expense = new Expense();
        $form = $this->createForm('HRManagementBundle\Form\ExpenseType', $expense);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($expense);
            $em->flush();

            return $this->redirectToRoute('expense_show', array('id' => $expense->getId()));
        }

        return $this->render('hr/management/expense/new.html.twig', array(
            'zone'=>'hr',
            'expense' => $expense,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Expense entity.
     *
     * @Route("/{id}", name="expense_show")
     * @Method("GET")
     *
     * @todo Ajouter formulaire de validation pour les utilisateurs autorisés
     */
    public function showAction(Expense $expense)
    {
        $deleteForm = $this->createDeleteForm($expense);

        return $this->render('hr/management/expense/show.html.twig', array(
            'zone'=>'hr',
            'expense' => $expense,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Expense entity.
     *
     * @Route("/{id}/edit", name="expense_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Expense $expense)
    {
        $deleteForm = $this->createDeleteForm($expense);
        $editForm = $this->createForm('HRManagementBundle\Form\ExpenseType', $expense);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($expense);
            $em->flush();

            return $this->redirectToRoute('expense_edit', array('id' => $expense->getId()));
        }

        return $this->render('hr/management/expense/edit.html.twig', array(
            'zone'=>'hr',
            'expense' => $expense,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Expense entity.
     *
     * @Route("/{id}", name="expense_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Expense $expense)
    {
        $form = $this->createDeleteForm($expense);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($expense);
            $em->flush();
        }

        return $this->redirectToRoute('expense_index');
    }

    /**
     * Creates a form to delete a Expense entity.
     *
     * @param Expense $expense The Expense entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Expense $expense)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('expense_delete', array('id' => $expense->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
