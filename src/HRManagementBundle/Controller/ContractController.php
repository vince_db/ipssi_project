<?php

namespace HRManagementBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use HRManagementBundle\Entity\Contract;
use HRManagementBundle\Form\ContractType;

/**
 * Contract controller.
 *
 * @Route("/hr/management/contract")
 *
 * @todo définir et implémenter le meilleir moyen de traduire les types de contrats (k => v)
 */
class ContractController extends Controller
{
    /**
     * Lists all Contract entities.
     *
     * @Route("/", name="contract_index")
     * @Method("GET")
     * @todo Afficher plus d'infos sur les utilisateurs liés (nom, prénom)
     * @todo NTH : Ajouter la date de 1ere embauche à tous les users liés aux comtrats affichés
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $contracts = $em->getRepository('HRManagementBundle:Contract')->findAll();

        return $this->render('hr/management/contract/index.html.twig', array(
            'zone'=>'hr',
            'contracts' => $contracts,
            'contractTypes' => array_flip(Contract::getTypes())
        ));
    }

    /**
     * Creates a new Contract entity.
     *
     * @Route("/new", name="contract_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $contract = new Contract();
        $form = $this->createForm('HRManagementBundle\Form\ContractType', $contract);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($contract);
            $em->flush();

            return $this->redirectToRoute('contract_show', array('id' => $contract->getId()));
        }

        return $this->render('hr/management/contract/new.html.twig', array(
            'zone'=>'hr',
            'contract' => $contract,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Contract entity.
     *
     * @Route("/{id}", name="contract_show")
     * @Method("GET")
     * @todo deporter traduction type dans un service
     */
    public function showAction(Contract $contract)
    {
        // Affichage du type de contrat en clair
        $contractTypes = array_flip($contract::getTypes());
        $contract->setType($contractTypes[$contract->getType()]);

        $deleteForm = $this->createDeleteForm($contract);

        return $this->render('hr/management/contract/show.html.twig', array(
            'zone'=>'hr',
            'contract' => $contract,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Contract entity.
     *
     * @Route("/{id}/edit", name="contract_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Contract $contract)
    {
        $deleteForm = $this->createDeleteForm($contract);
        $editForm = $this->createForm('HRManagementBundle\Form\ContractType', $contract);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($contract);
            $em->flush();

            return $this->redirectToRoute('contract_edit', array('id' => $contract->getId()));
        }

        return $this->render('hr/management/contract/edit.html.twig', array(
            'zone'=>'hr',
            'contract' => $contract,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Contract entity.
     *
     * @Route("/{id}", name="contract_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Contract $contract)
    {
        $form = $this->createDeleteForm($contract);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($contract);
            $em->flush();
        }

        return $this->redirectToRoute('contract_index');
    }

    /**
     * Creates a form to delete a Contract entity.
     *
     * @param Contract $contract The Contract entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Contract $contract)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('contract_delete', array('id' => $contract->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
