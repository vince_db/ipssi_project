<?php

namespace HRManagementBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/hr/management")
     */
    public function indexAction()
    {
        return $this->render('hr/index.html.twig');
    }
}
