<?php

namespace HRManagementBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use HRManagementBundle\Entity\HolidayRequest;
use HRManagementBundle\Form\HolidayRequestType;

/**
 * HolidayRequest controller.
 *
 * @Route("/hr/management/holiday/request")
 */
class HolidayRequestController extends Controller
{
    /**
     * Lists all HolidayRequest entities.
     *
     * @Route("/", name="holidayrequest_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $holidayRequests = $em->getRepository('HRManagementBundle:HolidayRequest')->findAll();

        return $this->render('hr/management/holiday/request/index.html.twig', array(
            'zone'=>'hr',
            'holidayRequests' => $holidayRequests,
        ));
    }

    /**
     * Creates a new HolidayRequest entity.
     *
     * @Route("/new", name="holidayrequest_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $holidayRequest = new HolidayRequest();
        $form = $this->createForm('HRManagementBundle\Form\HolidayRequestType', $holidayRequest);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($holidayRequest);
            $em->flush();

            return $this->redirectToRoute('holidayrequest_show', array('id' => $holidayRequest->getId()));
        }

        return $this->render('hr/management/holiday/request/new.html.twig', array(
            'zone'=>'hr',
            'holidayRequest' => $holidayRequest,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a HolidayRequest entity.
     *
     * @Route("/{id}", name="holidayrequest_show")
     * @Method("GET")
     */
    public function showAction(HolidayRequest $holidayRequest)
    {
        $deleteForm = $this->createDeleteForm($holidayRequest);

        return $this->render('hr/management/holiday/request/show.html.twig', array(
            'zone'=>'hr',
            'holidayRequest' => $holidayRequest,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing HolidayRequest entity.
     *
     * @Route("/{id}/edit", name="holidayrequest_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, HolidayRequest $holidayRequest)
    {
        $deleteForm = $this->createDeleteForm($holidayRequest);
        $editForm = $this->createForm('HRManagementBundle\Form\HolidayRequestType', $holidayRequest);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($holidayRequest);
            $em->flush();

            return $this->redirectToRoute('holidayrequest_edit', array('id' => $holidayRequest->getId()));
        }

        return $this->render('hr/management/holiday/request/edit.html.twig', array(
            'zone'=>'hr',
            'holidayRequest' => $holidayRequest,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a HolidayRequest entity.
     *
     * @Route("/{id}", name="holidayrequest_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, HolidayRequest $holidayRequest)
    {
        $form = $this->createDeleteForm($holidayRequest);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($holidayRequest);
            $em->flush();
        }

        return $this->redirectToRoute('holidayrequest_index');
    }

    /**
     * Creates a form to delete a HolidayRequest entity.
     *
     * @param HolidayRequest $holidayRequest The HolidayRequest entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(HolidayRequest $holidayRequest)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('holidayrequest_delete', array('id' => $holidayRequest->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
