<?php

namespace HRManagementBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use HRManagementBundle\Entity\HolidayApproval;
use HRManagementBundle\Form\HolidayApprovalType;

/**
 * HolidayApproval controller.
 *
 * @Route("/hr/management/holiday/approval")
 */
class HolidayApprovalController extends Controller
{
    /**
     * Lists all HolidayApproval entities.
     *
     * @Route("/", name="holidayapproval_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $holidayApprovals = $em->getRepository('HRManagementBundle:HolidayApproval')->findAll();

        return $this->render('hr/management/holiday/approval/index.html.twig', array(
            'zone'=>'hr',
            'holidayApprovals' => $holidayApprovals,
        ));
    }

    /**
     * Creates a new HolidayApproval entity.
     *
     * @Route("/new", name="holidayapproval_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $holidayApproval = new HolidayApproval();
        $form = $this->createForm('HRManagementBundle\Form\HolidayApprovalType', $holidayApproval);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($holidayApproval);
            $em->flush();

            return $this->redirectToRoute('holidayapproval_show', array('id' => $holidayApproval->getId()));
        }

        return $this->render('hr/management/holiday/approval/new.html.twig', array(
            'zone'=>'hr',
            'holidayApproval' => $holidayApproval,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a HolidayApproval entity.
     *
     * @Route("/{id}", name="holidayapproval_show")
     * @Method("GET")
     */
    public function showAction(HolidayApproval $holidayApproval)
    {
        $deleteForm = $this->createDeleteForm($holidayApproval);

        return $this->render('hr/management/holiday/approval/show.html.twig', array(
            'zone'=>'hr',
            'holidayApproval' => $holidayApproval,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing HolidayApproval entity.
     *
     * @Route("/{id}/edit", name="holidayapproval_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, HolidayApproval $holidayApproval)
    {
        $deleteForm = $this->createDeleteForm($holidayApproval);
        $editForm = $this->createForm('HRManagementBundle\Form\HolidayApprovalType', $holidayApproval);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($holidayApproval);
            $em->flush();

            return $this->redirectToRoute('holidayapproval_edit', array('id' => $holidayApproval->getId()));
        }

        return $this->render('hr/management/holiday/approval/edit.html.twig', array(
            'zone'=>'hr',
            'holidayApproval' => $holidayApproval,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a HolidayApproval entity.
     *
     * @Route("/{id}", name="holidayapproval_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, HolidayApproval $holidayApproval)
    {
        $form = $this->createDeleteForm($holidayApproval);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($holidayApproval);
            $em->flush();
        }

        return $this->redirectToRoute('holidayapproval_index');
    }

    /**
     * Creates a form to delete a HolidayApproval entity.
     *
     * @param HolidayApproval $holidayApproval The HolidayApproval entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(HolidayApproval $holidayApproval)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('holidayapproval_delete', array('id' => $holidayApproval->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
