<?php

namespace HRManagementBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use HRManagementBundle\Entity\ExpenseValidation;
use HRManagementBundle\Form\ExpenseValidationType;

/**
 * ExpenseValidation controller.
 *
 * @Route("/hr/management/expense/validation")
 */
class ExpenseValidationController extends Controller
{
    /**
     * Lists all ExpenseValidation entities.
     *
     * @Route("/", name="expensevalidation_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $expenseValidations = $em->getRepository('HRManagementBundle:ExpenseValidation')->findAll();

        return $this->render('hr/management/expense/validation/index.html.twig', array(
            'zone'=>'hr',
            'expenseValidations' => $expenseValidations,
        ));
    }

    /**
     * Creates a new ExpenseValidation entity.
     *
     * @Route("/new", name="expensevalidation_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $expenseValidation = new ExpenseValidation();
        $form = $this->createForm('HRManagementBundle\Form\ExpenseValidationType', $expenseValidation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($expenseValidation);
            $em->flush();

            return $this->redirectToRoute('expensevalidation_show', array('id' => $expenseValidation->getId()));
        }

        return $this->render('hr/management/expense/validation/new.html.twig', array(
            'zone'=>'hr',
            'expenseValidation' => $expenseValidation,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a ExpenseValidation entity.
     *
     * @Route("/{id}", name="expensevalidation_show")
     * @Method("GET")
     */
    public function showAction(ExpenseValidation $expenseValidation)
    {
        $deleteForm = $this->createDeleteForm($expenseValidation);

        return $this->render('hr/management/expense/validation/show.html.twig', array(
            'zone'=>'hr',
            'expenseValidation' => $expenseValidation,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing ExpenseValidation entity.
     *
     * @Route("/{id}/edit", name="expensevalidation_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, ExpenseValidation $expenseValidation)
    {
        $deleteForm = $this->createDeleteForm($expenseValidation);
        $editForm = $this->createForm('HRManagementBundle\Form\ExpenseValidationType', $expenseValidation);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($expenseValidation);
            $em->flush();

            return $this->redirectToRoute('expensevalidation_edit', array('id' => $expenseValidation->getId()));
        }

        return $this->render('hr/management/expense/validation/edit.html.twig', array(
            'zone'=>'hr',
            'expenseValidation' => $expenseValidation,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a ExpenseValidation entity.
     *
     * @Route("/{id}", name="expensevalidation_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, ExpenseValidation $expenseValidation)
    {
        $form = $this->createDeleteForm($expenseValidation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($expenseValidation);
            $em->flush();
        }

        return $this->redirectToRoute('expensevalidation_index');
    }

    /**
     * Creates a form to delete a ExpenseValidation entity.
     *
     * @param ExpenseValidation $expenseValidation The ExpenseValidation entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(ExpenseValidation $expenseValidation)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('expensevalidation_delete', array('id' => $expenseValidation->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
