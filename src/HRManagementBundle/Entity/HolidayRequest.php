<?php

namespace HRManagementBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * HolidayRequest
 *
 * @ORM\Table(name="holiday_request")
 * @ORM\Entity(repositoryClass="HRManagementBundle\Repository\HolidayRequestRepository")
 */
class HolidayRequest
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     */
    private $user;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_start", type="date")
     */
    private $dateStart;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_end", type="date")
     */
    private $dateEnd;

    /**
     * @var bool
     *
     * @ORM\Column(name="paid", type="boolean")
     */
    private $paid = true;

    /**
     * @ORM\OneToOne(targetEntity="HRManagementBundle\Entity\HolidayApproval", inversedBy="holidayRequest")
     */
    private $holidayApproval;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param string $user
     *
     * @return HolidayRequest
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return string
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set dateStart
     *
     * @param \DateTime $dateStart
     *
     * @return HolidayRequest
     */
    public function setDateStart($dateStart)
    {
        $this->dateStart = $dateStart;

        return $this;
    }

    /**
     * Get dateStart
     *
     * @return \DateTime
     */
    public function getDateStart()
    {
        return $this->dateStart;
    }

    /**
     * Set dateEnd
     *
     * @param \DateTime $dateEnd
     *
     * @return HolidayRequest
     */
    public function setDateEnd($dateEnd)
    {
        $this->dateEnd = $dateEnd;

        return $this;
    }

    /**
     * Get dateEnd
     *
     * @return \DateTime
     */
    public function getDateEnd()
    {
        return $this->dateEnd;
    }

    /**
     * Set paid
     *
     * @param boolean $paid
     *
     * @return HolidayRequest
     */
    public function setPaid($paid)
    {
        $this->paid = $paid;

        return $this;
    }

    /**
     * Get paid
     *
     * @return bool
     */
    public function getPaid()
    {
        return $this->paid;
    }

    /**
     * @return mixed
     */
    public function getHolidayApproval()
    {
        return $this->holidayApproval;
    }

    /**
     * @param mixed $holidayApproval
     * @return HolidayRequest
     */
    public function setHolidayApproval($holidayApproval)
    {
        $this->holidayApproval = $holidayApproval;
        return $this;
    }

}

