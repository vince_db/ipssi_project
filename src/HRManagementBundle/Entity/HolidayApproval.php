<?php

namespace HRManagementBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * HolidayApproval
 *
 * @ORM\Table(name="holiday_approval")
 * @ORM\Entity(repositoryClass="HRManagementBundle\Repository\HolidayApprovalRepository")
 */
class HolidayApproval
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="HRManagementBundle\Entity\HolidayRequest", mappedBy="holidayApproval")
     */
    private $holidayRequest;

    /**
     * @var string
     *
     * Auteur de la validation de congés
     *
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     */
    private $user;

    /**
     * @var bool
     *
     * @ORM\Column(name="approved", type="boolean")
     */
    private $approved;

    /**
     * @var string
     *
     * @ORM\Column(name="refusalReason", type="string", length=510, nullable=true)
     * @ORM\Column(name="refusal_reason")
     */
    private $refusalReason;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set holidayRequest
     *
     * @param string $holidayRequest
     *
     * @return HolidayApproval
     */
    public function setHolidayRequest($holidayRequest)
    {
        $this->holidayRequest = $holidayRequest;

        return $this;
    }

    /**
     * Get holidayRequest
     *
     * @return string
     */
    public function getHolidayRequest()
    {
        return $this->holidayRequest;
    }

    /**
     * Set user
     *
     * @param string $user
     *
     * @return HolidayApproval
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return string
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set approved
     *
     * @param boolean $approved
     *
     * @return HolidayApproval
     */
    public function setApproved($approved)
    {
        $this->approved = $approved;

        return $this;
    }

    /**
     * Get approved
     *
     * @return bool
     */
    public function getApproved()
    {
        return $this->approved;
    }

    /**
     * Set refusalReason
     *
     * @param string $refusalReason
     *
     * @return HolidayApproval
     */
    public function setRefusalReason($refusalReason)
    {
        $this->refusalReason = $refusalReason;

        return $this;
    }

    /**
     * Get refusalReason
     *
     * @return string
     */
    public function getRefusalReason()
    {
        return $this->refusalReason;
    }
}

