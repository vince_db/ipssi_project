<?php

namespace HRManagementBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ExpenseValidation
 *
 * @ORM\Table(name="expense_validation")
 * @ORM\Entity(repositoryClass="HRManagementBundle\Repository\ExpenseValidationRepository")
 */
class ExpenseValidation
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\OneToOne(targetEntity="HRManagementBundle\Entity\Expense", inversedBy="validation")
     */
    private $expense;

    /**
     * @var bool
     *
     * @ORM\Column(name="accepted", type="boolean")
     */
    private $accepted;

    /**
     * @var string
     *
     * @ORM\Column(name="reasonRefused", type="string", length=510, nullable=true)
     */
    private $reasonRefused;

    /**
     * @var string
     *
     * Auteur de la validation = RH validant la note de frais
     *
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     */
    private $user;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set expense
     *
     * @param string $expense
     *
     * @return ExpenseValidation
     */
    public function setExpense($expense)
    {
        $this->expense = $expense;

        return $this;
    }

    /**
     * Get expense
     *
     * @return string
     */
    public function getExpense()
    {
        return $this->expense;
    }

    /**
     * Set accepted
     *
     * @param boolean $accepted
     *
     * @return ExpenseValidation
     */
    public function setAccepted($accepted)
    {
        $this->accepted = $accepted;

        return $this;
    }

    /**
     * Get accepted
     *
     * @return bool
     */
    public function getAccepted()
    {
        return $this->accepted;
    }

    /**
     * Set reasonRefused
     *
     * @param string $reasonRefused
     *
     * @return ExpenseValidation
     */
    public function setReasonRefused($reasonRefused)
    {
        $this->reasonRefused = $reasonRefused;

        return $this;
    }

    /**
     * Get reasonRefused
     *
     * @return string
     */
    public function getReasonRefused()
    {
        return $this->reasonRefused;
    }

    /**
     * @return string
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param string $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

}

