<?php

namespace HRManagementBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Contract
 *
 * @ORM\Table(name="contract")
 * @ORM\Entity(repositoryClass="HRManagementBundle\Repository\ContractRepository")
 */
class Contract
{
    const TYPE_PERM = "CDI";
    const TYPE_FIX = "CDD";
    const TYPE_TEMP = "Temporaire";
    const TYPE_INT = "Stage";

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=120, unique=true)
     */
    private $type;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_start", type="date")
     */
    private $dateStart;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_end", type="date", nullable=true)
     */
    private $dateEnd;

    /**
     * @var int
     *
     * @ORM\Column(name="weekly_hours", type="integer")
     */
    private $weeklyHours;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     * @return Contract
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Contract
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set dateStart
     *
     * @param \DateTime $dateStart
     *
     * @return Contract
     */
    public function setDateStart($dateStart)
    {
        $this->dateStart = $dateStart;

        return $this;
    }

    /**
     * Get dateStart
     *
     * @return \DateTime
     */
    public function getDateStart()
    {
        return $this->dateStart;
    }

    /**
     * Set dateEnd
     *
     * @param \DateTime $dateEnd
     *
     * @return Contract
     */
    public function setDateEnd($dateEnd)
    {
        $this->dateEnd = $dateEnd;

        return $this;
    }

    /**
     * Get dateEnd
     *
     * @return \DateTime
     */
    public function getDateEnd()
    {
        return $this->dateEnd;
    }

    /**
     * Set weeklyHours
     *
     * @param integer $weeklyHours
     *
     * @return Contract
     */
    public function setWeeklyHours($weeklyHours)
    {
        $this->weeklyHours = $weeklyHours;

        return $this;
    }

    /**
     * Get weeklyHours
     *
     * @return int
     */
    public function getWeeklyHours()
    {
        return $this->weeklyHours;
    }


    /**
     * Retourne types possibles de contrats
     * @return array
     * @todo best practice ? extract in another object (value-object) ?
     */
    public static function getTypes()
    {
        return [
            self::TYPE_PERM => 'TYPE_PERM',
            self::TYPE_FIX => 'TYPE_FIX',
            self::TYPE_TEMP => 'TYPE_TEMP',
            self::TYPE_INT => 'TYPE_INT',
        ];
    }

}

