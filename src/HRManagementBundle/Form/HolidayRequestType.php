<?php

namespace HRManagementBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class HolidayRequestType
 * @package HRManagementBundle\Form
 * @todo Trouver comment passer l'ID user de façon transparente pour le front (controller? service?)
 * @todo Supprimer ou cacher les champs user de HolidayRequest et HolidayApproval
 */
class HolidayRequestType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dateStart', DateType::class, array(
                // render as a single text box
                'widget' => 'single_text',
            ))
            ->add('dateEnd', DateType::class, array(
                // render as a single text box
                'widget' => 'single_text',
            ))
            ->add('paid')
            ->add('user', EntityType::class, [
                'class' => 'UserBundle\Entity\User',
                'choice_value' => 'id',
                'choice_label' => 'lastname'
            ])
            // ->add('holidayApproval', HolidayApprovalType::class)
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'HRManagementBundle\Entity\HolidayRequest'
        ));
    }
}
