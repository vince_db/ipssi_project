<?php

namespace HRManagementBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ExpenseValidationType
 * Formulaire de validation de note de frais
 * @package HRManagementBundle\Form
 *
 * @todo Passer l'utilisateur courant au persist par une méthode à determiner (controller, service, Lifecycle callback)
 */
class ExpenseValidationType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('accepted')
            ->add('reasonRefused')
            ->add('expense', EntityType::class, [
                'class' => 'HRManagementBundle\Entity\Expense',
                'choice_value' => 'id',
                'choice_label' => 'name'
            ])
            ->add('user', EntityType::class, [
                'class' => 'UserBundle\Entity\User',
                'choice_value' => 'id',
                'choice_label' => 'lastname'
            ])
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'HRManagementBundle\Entity\ExpenseValidation'
        ));
    }
}
