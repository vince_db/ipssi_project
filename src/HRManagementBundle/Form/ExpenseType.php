<?php

namespace HRManagementBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ExpenseType
 * Formulaire note de frais
 * @package HRManagementBundle\Form
 *
 * @todo Passer l'utilisateur courant au persist par une méthode à determiner (controller, service, Lifecycle callback)
 */
class ExpenseType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('date', DateType::class, array(
                // render as a single text box
                'widget' => 'single_text',
            ))
            ->add('name')
            ->add('comment')
            ->add('amount')
            ->add('proof')
            ->add('user', EntityType::class, [
                'class' => 'UserBundle\Entity\User',
                'choice_value' => 'id',
                'choice_label' => 'lastname'
            ])
            // ->add('validation')
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'HRManagementBundle\Entity\Expense'
        ));
    }
}
