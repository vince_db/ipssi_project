<?php

namespace HRManagementBundle\Form;

use HRManagementBundle\Entity\Contract;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContractType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('user', EntityType::class, [
                'class' => 'UserBundle\Entity\User',
                'choice_value' => 'id',
                'choice_label' => 'lastname'
            ])
            ->add('type', ChoiceType::class, [
                'choices' => Contract::getTypes()
            ])
            ->add('dateStart', DateType::class)
            ->add('dateEnd', DateType::class)
            ->add('weeklyHours')
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'HRManagementBundle\Entity\Contract'
        ));
    }
}
