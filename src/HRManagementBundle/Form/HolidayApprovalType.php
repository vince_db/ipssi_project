<?php

namespace HRManagementBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class HolidayApprovalType
 * @package HRManagementBundle\Form
 * @todo Passer l'id user (auteur) de façon transparente
 * @todo Revoir la présentation de HolidayRequest
 */
class HolidayApprovalType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('approved')
            ->add('refusalReason')
            ->add('holidayRequest', EntityType::class, [
                'class' => 'HRManagementBundle\Entity\HolidayRequest',
                'choice_value' => 'id',
                'choice_label' => 'id'
            ])
            ->add('user', EntityType::class, [
                'class' => 'UserBundle\Entity\User',
                'choice_value' => 'id',
                'choice_label' => 'lastname'
            ])
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'HRManagementBundle\Entity\HolidayApproval'
        ));
    }
}
