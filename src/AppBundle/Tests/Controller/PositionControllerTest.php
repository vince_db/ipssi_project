<?php

namespace AppBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PositionControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/index');
    }

    public function testShow()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/show');
    }

    public function testAutocandidate()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/Autocandidate');
    }

    public function testCandidate()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/Candidate');
    }

}
