<?php

namespace AppBundle\Controller;

use AppBundle\Form\ContactType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class DefaultController
 * Tout ce qui concerne le site public
 *
 * @package AppBundle\Controller
 */
class DefaultController extends \AppBundle\Controller\PublicLayoutController
{
    /**
     * @Route("/", name="public_homepage")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $positions = $em->getRepository('HRSourcingBundle:Position')->findAll(4);

        return $this->render(':public_zone:public_template.html.twig', [
            'title' => 'Bienvenue chez IPSSI',
            'links' => $this->getLinks($em),
            'lastarticles' => $this->getArticles($em, 4),
            'datasite' => $this->getDatasite($em),
            'positions' => $positions,
            'articles' => $this->getArticles($em, 3),
            'welcome' => $this->getWelcome($em),
            'welcomeword' => $this->getWelcomeword($em),
            'welcomeimg' => $this->getWelcomeimg($em),
        ]);
    }

    /**
     * Page contact
     *
     * @Route("/contact", name="contact_page")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function contactAction(Request $request)
    {
        $form = $this->createForm(ContactType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $message = \Swift_Message::newInstance()
                ->setSubject($form->getData()['subject'])
                ->setFrom($form->getData()['email'])
                ->setTo('contact@ipssi.com')
                ->setBody(
                    $this->renderView(':public/contact:mail_template.html.twig', [
                        'firstname' => $form->getData()['firstname'],
                        'lastname' => $form->getData()['lastname'],
                        'email' => $form->getData()['email'],
                        'subject' => $form->getData()['subject'],
                        'message' => $form->getData()['message'],
                        'date' => date('m/d/Y H:i')
                    ])
                )
            ;

            $this->get('mailer')->send($message);
        }

        return $this->render('public/contact/contact_form.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
