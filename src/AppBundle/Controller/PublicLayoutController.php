<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class PublicLayoutController extends Controller
{
    /**
     * @ todo récupérer les données dans le back
     */
    protected function getLinks($em)
    {
        $links=[
          'facebook' => [
              'icon'=>'fa fa-facebook',
              'link' => 'id.facebook.com'
          ],
            'google' => [
                'icon'=>'fa fa-google-plus',
                'link' => 'www.google.fr'
            ],
            'tweeter' => [
                'icon' => 'fa fa-twitter',
                'link' => 'www.tweeter.com'
            ]
        ];

        return $links;
    }


    protected function getArticles($em, $num){
        $num=(int)$num;
        $articles = $em->getRepository('CMSBundle:Article')->findBy([], ['dateCreate' => 'DESC'], $num);
        return $articles;

    }

    /**
     * @ todo récupérer les données dans le back
     */
    protected function getDatasite($em)
    {
        $content=[
          'whoareus'=>'IPSSI est une société de consultants',
          'legal' => 'Le site, son design, et tout son contenu sont la propriété exclusive du groupe IPSSI. ',
            'address' => 'La seyne sur mer',
            'tel' =>'08-22-75-77-89',
            'mail' => 'groupipssi@gmail.com',
            'fax' => "04-25-36-11-55",
            'opening' => "lundi-vendredi 09h-12h 14h-19h30"
        ];

        return $content;
    }

    /**
     * @ todo récupérer les données dans le back
     */
    protected function getWelcome($em)
    {
        $content="Fondée en l'an de grâce 1497, la société Ippsi a commencé sa carrière de consultant en aiguillant les chevaliers sur leurs choix d'armures
        De nombreuses années plus tard, aptès avoir servi de consultants pour la sainte inquisition lors de la création des vierges de fer, nous avons eu l'opportunité de participer à la confection de la guillotine!
        
        Désormais, ce savoir faire ancestral, nous souhaitons le mettre à votre service!
        ";

        return $content;
    }

    /**
     * @ todo récupérer les données dans le back
     */
    protected function getWelcomeword($em)
    {
        $content="IPSSI et la torture, un savoir faire millénaire";

        return $content;
    }

    /**
     * @ todo récupérer les données dans le back
     */
    protected function getWelcomeimg($em)
    {
        $content="purty_wood.png";

        return $content;
    }

}
