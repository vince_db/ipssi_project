<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DashboardController extends Controller
{
    /**
     * @Route("/private", name="dashboard_show")
     */
    public function showAction()
    {
        return $this->render(':user:dashboard/show.html.twig', array(
            'zone'=>'toolbox',
            'title'=>'Mon tableau de bord'
            // ...
        ));
    }

}
