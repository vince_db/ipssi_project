<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class PositionController extends \AppBundle\Controller\PublicLayoutController
{
    /**
     * @Route("/public/position/index", name="public_position_index")
     */
    public function indexAction()
    {

        $em = $this->getDoctrine()->getManager();
        $positions = $em->getRepository('HRSourcingBundle:Position')->findAll(4);

        return $this->render(':public:position/index.html.twig', array(
            'title' => 'Les postes à pourvoir',
            'links' => $this->getLinks($em),
            'lastarticles' => $this->getArticles($em, 4),
            'datasite' => $this->getDatasite($em),
            'articles' => $this->getArticles($em, 3),

            'position' => $positions,
        ));
    }

    /**
     * @Route("/show")
     */
    public function showAction()
    {
        return $this->render(':public:position/show.html.twig', array(
            // ...
        ));
    }

    /**
     * @Route("/public/position/autocandidate", name="public_position_autocandidate")
     */
    public function AutocandidateAction()
    {
        $em = $this->getDoctrine()->getManager();

        return $this->render(':public:position/autocandidate.html.twig', array(
            'title' => 'Les postes à pourvoir',
            'links' => $this->getLinks($em),
            'lastarticles' => $this->getArticles($em, 4),
            'datasite' => $this->getDatasite($em),
            'articles' => $this->getArticles($em, 3),
        ));
    }

    /**
     * @Route("/Candidate")
     */
    public function CandidateAction()
    {
        return $this->render(':public:position/:candidate.html.twig', array(
            // ...
        ));
    }

}
