<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Parameter;
use AppBundle\Form\ParameterType;

/**
 * Parameter controller.
 *
 * @Route("/parameter")
 */
class ParameterController extends Controller
{
    /**
     * Finds and displays a Parameter entity.
     *
     * @Route("/{id}", name="parameter_show")
     * @Method("GET")
     */
    public function showAction(Parameter $parameter)
    {
        return $this->render('parameter/site/show.html.twig', array(
            'parameter' => $parameter,
        ));
    }

    /**
     * Displays a form to edit an existing Parameter entity.
     *
     * @Route("/{id}/edit", name="parameter_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Parameter $parameter)
    {
        $editForm = $this->createForm('AppBundle\Form\ParameterType', $parameter);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($parameter);
            $em->flush();

            return $this->redirectToRoute('parameter_edit', array('id' => $parameter->getId()));
        }

        return $this->render('parameter/site/edit.html.twig', array(
            'parameter' => $parameter,
            'edit_form' => $editForm->createView(),
        ));
    }
}
