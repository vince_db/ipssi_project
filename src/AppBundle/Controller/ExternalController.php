<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\External;
use AppBundle\Form\ExternalType;

/**
 * External controller.
 *
 * @Route("/external")
 */
class ExternalController extends Controller
{
    /**
     * Lists all External entities.
     *
     * @Route("/", name="external_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $externals = $em->getRepository('AppBundle:External')->findAll();

        return $this->render('parameter/external/index.html.twig', array(
            'zone' => 'params',
            'externals' => $externals,
        ));
    }

    /**
     * Creates a new External entity.
     *
     * @Route("/new", name="external_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $external = new External();
        $form = $this->createForm('AppBundle\Form\ExternalType', $external);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($external);
            $em->flush();

            return $this->redirectToRoute('external_show', array('id' => $external->getId()));
        }

        return $this->render('parameter/external/new.html.twig', array(
            'zone' => 'params',
            'external' => $external,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a External entity.
     *
     * @Route("/{id}", name="external_show")
     * @Method("GET")
     */
    public function showAction(External $external)
    {
        $deleteForm = $this->createDeleteForm($external);

        return $this->render('parameter/external/show.html.twig', array(
            'zone' => 'params',
            'external' => $external,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing External entity.
     *
     * @Route("/{id}/edit", name="external_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, External $external)
    {
        $deleteForm = $this->createDeleteForm($external);
        $editForm = $this->createForm('AppBundle\Form\ExternalType', $external);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($external);
            $em->flush();

            return $this->redirectToRoute('external_edit', array('id' => $external->getId()));
        }

        return $this->render('parameter/external/edit.html.twig', array(
            'zone' => 'params',
            'external' => $external,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a External entity.
     *
     * @Route("/{id}", name="external_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, External $external)
    {
        $form = $this->createDeleteForm($external);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($external);
            $em->flush();
        }

        return $this->redirectToRoute('external_index');
    }

    /**
     * Creates a form to delete a External entity.
     *
     * @param External $external The External entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(External $external)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('external_delete', array('id' => $external->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
