<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ContactType
 * Formulaire de contact zone publique
 *
 * @package AppBundle\Form
 */
class ContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('subject')
            ->add('firstname')
            ->add('lastname')
            ->add('email')
            ->add('message')
        ;
    }

    public function setDefaultOptions(OptionsResolver $resolver)
    {}
}