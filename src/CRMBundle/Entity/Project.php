<?php

namespace CRMBundle\Entity;

use AppBundle\Traits\TimestampableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Project
 *
 * @ORM\Table(name="project")
 * @ORM\Entity(repositoryClass="CRMBundle\Repository\ProjectRepository")
 */
class Project
{
    // @todo trait Timestampable introuvable
    // use TimestampableTrait;

    /**
     * Project constructor.
     */
    public function __construct()
    {
        $this->consultants = new ArrayCollection();
        $this->activityReports = new ArrayCollection();
    }

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="startAt", type="datetime")
     */
    private $startAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="endAt", type="datetime")
     */
    private $endAt;

    /**
     * @var string
     *
     * @ORM\Column(name="client", type="string", length=255)
     */
    private $client;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Address")
     */
    private $address;

    /**
     * @ORM\OneToOne(targetEntity="CRMBundle\Entity\ClientContact")
     */
    private $contact;

    /**
     * @ORM\OneToMany(targetEntity="UserBundle\Entity\User")
     */

    /**
     * Table de jointure projet et user : permet de connaitre les utilisateurs assignés à un projet sans avoir à
     * poluer le UserBundle avec de la logique ne le concernant pas
     *
     * @ORM\ManyToMany(targetEntity="UserBundle\Entity\User")
     * @ORM\JoinTable(name="project_consultant",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="project_id", referencedColumnName="id", unique=true)}
     *      )
     */
    private $consultants;

    /**
     * @ORM\OneToMany(targetEntity="CRMBundle\Entity\ActivityReport", mappedBy="project")
     */
    private $activityReports;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Project
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set startAt
     *
     * @param \DateTime $startAt
     *
     * @return Project
     */
    public function setStartAt($startAt)
    {
        $this->startAt = $startAt;

        return $this;
    }

    /**
     * Get startAt
     *
     * @return \DateTime
     */
    public function getStartAt()
    {
        return $this->startAt;
    }

    /**
     * Set endAt
     *
     * @param \DateTime $endAt
     *
     * @return Project
     */
    public function setEndAt($endAt)
    {
        $this->endAt = $endAt;

        return $this;
    }

    /**
     * Get endAt
     *
     * @return \DateTime
     */
    public function getEndAt()
    {
        return $this->endAt;
    }

    /**
     * Set client
     *
     * @param string $client
     *
     * @return Project
     */
    public function setClient($client)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return string
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set siteAddress
     *
     * @param string $address
     *
     * @return Project
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get siteAddress
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set contact
     *
     * @param string $contact
     *
     * @return Project
     */
    public function setContact($contact)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get contact
     *
     * @return string
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Set consultants
     *
     * @param string $consultants
     *
     * @return Project
     */
    public function setConsultants($consultants)
    {
        $this->consultants = $consultants;

        return $this;
    }

    /**
     * Get consultants
     *
     * @return string
     */
    public function getConsultants()
    {
        return $this->consultants;
    }

    /**
     * @return mixed
     */
    public function getActivityReports()
    {
        return $this->activityReports;
    }

    /**
     * @param mixed $activityReports
     */
    public function setActivityReports($activityReports)
    {
        $this->activityReports = $activityReports;
    }

}

