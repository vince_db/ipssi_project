<?php

namespace CRMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ActivityReport
 *
 * @ORM\Table(name="activity_report")
 * @ORM\Entity(repositoryClass="CRMBundle\Repository\ActivityReportRepository")
 */
class ActivityReport
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="startAt", type="datetime")
     */
    private $startAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="endAt", type="datetime")
     */
    private $endAt;

    /**
     * @ORM\ManyToOne(targetEntity="CRMBundle\Entity\Project", inversedBy="activityReports")
     */
    private $project;

    /**
     * @var int
     *
     * @ORM\Column(name="numberAccidentsWithStoppage", type="smallint", nullable=true)
     */
    private $numberAccidentsWithStoppage;

    /**
     * @var int
     *
     * @ORM\Column(name="numberAccidentsWithoutStoppage", type="smallint", nullable=true)
     */
    private $numberAccidentsWithoutStoppage;

    /**
     * @var int
     *
     * @ORM\Column(name="numberAccidentsOnWay", type="smallint", nullable=true)
     */
    private $numberAccidentsOnWay;

    /**
     * @var int
     *
     * @ORM\Column(name="numberAlmostAccidents", type="smallint", nullable=true)
     */
    private $numberAlmostAccidents;

    /**
     * @var int
     *
     * @ORM\Column(name="numberMedicalLeaves", type="smallint", nullable=true)
     */
    private $numberMedicalLeaves;

    /**
     * @var int
     *
     * @ORM\Column(name="consultantStatisfaction", type="smallint", nullable=true)
     */
    private $consultantStatisfaction;

    /**
     * @var int
     *
     * @ORM\Column(name="clientSatisfaction", type="smallint", nullable=true)
     */
    private $clientSatisfaction;

    /**
     * @var string
     *
     * @ORM\Column(name="possibleImprovements", type="string", length=255, nullable=true)
     */
    private $possibleImprovements;

    /**
     * @var string
     *
     * @ORM\Column(name="remainingTasks", type="string", length=255, nullable=true)
     */
    private $remainingTasks;

    /**
     * @var string
     *
     * @ORM\Column(name="clientComments", type="string", length=255, nullable=true)
     */
    private $clientComments;

    /**
     * @var string
     *
     * @ORM\Column(name="consultantComments", type="string", length=255, nullable=true)
     */
    private $consultantComments;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set startAt
     *
     * @param \DateTime $startAt
     *
     * @return ActivityReport
     */
    public function setStartAt($startAt)
    {
        $this->startAt = $startAt;

        return $this;
    }

    /**
     * Get startAt
     *
     * @return \DateTime
     */
    public function getStartAt()
    {
        return $this->startAt;
    }

    /**
     * Set endAt
     *
     * @param \DateTime $endAt
     *
     * @return ActivityReport
     */
    public function setEndAt($endAt)
    {
        $this->endAt = $endAt;

        return $this;
    }

    /**
     * Get endAt
     *
     * @return \DateTime
     */
    public function getEndAt()
    {
        return $this->endAt;
    }

    /**
     * Set project
     *
     * @param string $project
     *
     * @return ActivityReport
     */
    public function setProject($project)
    {
        $this->project = $project;

        return $this;
    }

    /**
     * Get project
     *
     * @return string
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * Set numberAccidentsWithStoppage
     *
     * @param integer $numberAccidentsWithStoppage
     *
     * @return ActivityReport
     */
    public function setNumberAccidentsWithStoppage($numberAccidentsWithStoppage)
    {
        $this->numberAccidentsWithStoppage = $numberAccidentsWithStoppage;

        return $this;
    }

    /**
     * Get numberAccidentsWithStoppage
     *
     * @return int
     */
    public function getNumberAccidentsWithStoppage()
    {
        return $this->numberAccidentsWithStoppage;
    }

    /**
     * Set numberAccidentsWithoutStoppage
     *
     * @param integer $numberAccidentsWithoutStoppage
     *
     * @return ActivityReport
     */
    public function setNumberAccidentsWithoutStoppage($numberAccidentsWithoutStoppage)
    {
        $this->numberAccidentsWithoutStoppage = $numberAccidentsWithoutStoppage;

        return $this;
    }

    /**
     * Get numberAccidentsWithoutStoppage
     *
     * @return int
     */
    public function getNumberAccidentsWithoutStoppage()
    {
        return $this->numberAccidentsWithoutStoppage;
    }

    /**
     * Set numberAccidentsOnWay
     *
     * @param integer $numberAccidentsOnWay
     *
     * @return ActivityReport
     */
    public function setNumberAccidentsOnWay($numberAccidentsOnWay)
    {
        $this->numberAccidentsOnWay = $numberAccidentsOnWay;

        return $this;
    }

    /**
     * Get numberAccidentsOnWay
     *
     * @return int
     */
    public function getNumberAccidentsOnWay()
    {
        return $this->numberAccidentsOnWay;
    }

    /**
     * Set numberAlmostAccidents
     *
     * @param integer $numberAlmostAccidents
     *
     * @return ActivityReport
     */
    public function setNumberAlmostAccidents($numberAlmostAccidents)
    {
        $this->numberAlmostAccidents = $numberAlmostAccidents;

        return $this;
    }

    /**
     * Get numberAlmostAccidents
     *
     * @return int
     */
    public function getNumberAlmostAccidents()
    {
        return $this->numberAlmostAccidents;
    }

    /**
     * Set numberMedicalLeaves
     *
     * @param integer $numberMedicalLeaves
     *
     * @return ActivityReport
     */
    public function setNumberMedicalLeaves($numberMedicalLeaves)
    {
        $this->numberMedicalLeaves = $numberMedicalLeaves;

        return $this;
    }

    /**
     * Get numberMedicalLeaves
     *
     * @return int
     */
    public function getNumberMedicalLeaves()
    {
        return $this->numberMedicalLeaves;
    }

    /**
     * Set consultantStatisfaction
     *
     * @param integer $consultantStatisfaction
     *
     * @return ActivityReport
     */
    public function setConsultantStatisfaction($consultantStatisfaction)
    {
        $this->consultantStatisfaction = $consultantStatisfaction;

        return $this;
    }

    /**
     * Get consultantStatisfaction
     *
     * @return int
     */
    public function getConsultantStatisfaction()
    {
        return $this->consultantStatisfaction;
    }

    /**
     * Set clientSatisfaction
     *
     * @param integer $clientSatisfaction
     *
     * @return ActivityReport
     */
    public function setClientSatisfaction($clientSatisfaction)
    {
        $this->clientSatisfaction = $clientSatisfaction;

        return $this;
    }

    /**
     * Get clientSatisfaction
     *
     * @return int
     */
    public function getClientSatisfaction()
    {
        return $this->clientSatisfaction;
    }

    /**
     * Set possibleImprovements
     *
     * @param string $possibleImprovements
     *
     * @return ActivityReport
     */
    public function setPossibleImprovements($possibleImprovements)
    {
        $this->possibleImprovements = $possibleImprovements;

        return $this;
    }

    /**
     * Get possibleImprovements
     *
     * @return string
     */
    public function getPossibleImprovements()
    {
        return $this->possibleImprovements;
    }

    /**
     * Set remainingTasks
     *
     * @param string $remainingTasks
     *
     * @return ActivityReport
     */
    public function setRemainingTasks($remainingTasks)
    {
        $this->remainingTasks = $remainingTasks;

        return $this;
    }

    /**
     * Get remainingTasks
     *
     * @return string
     */
    public function getRemainingTasks()
    {
        return $this->remainingTasks;
    }

    /**
     * Set clientComments
     *
     * @param string $clientComments
     *
     * @return ActivityReport
     */
    public function setClientComments($clientComments)
    {
        $this->clientComments = $clientComments;

        return $this;
    }

    /**
     * Get clientComments
     *
     * @return string
     */
    public function getClientComments()
    {
        return $this->clientComments;
    }

    /**
     * Set consultantComments
     *
     * @param string $consultantComments
     *
     * @return ActivityReport
     */
    public function setConsultantComments($consultantComments)
    {
        $this->consultantComments = $consultantComments;

        return $this;
    }

    /**
     * Get consultantComments
     *
     * @return string
     */
    public function getConsultantComments()
    {
        return $this->consultantComments;
    }
}

