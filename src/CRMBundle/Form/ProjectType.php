<?php

namespace CRMBundle\Form;

use AppBundle\Form\AddressType;
use CRMBundle\Entity\Client;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use UserBundle\Entity\User;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class ProjectType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('startAt', DateType::class, array(
                // render as a single text box
                'widget' => 'single_text',
            ))
            ->add('endAt', DateType::class, array(
                // render as a single text box
                'widget' => 'single_text',
            ))
            ->add('client', EntityType::class, [
                'class' => Client::class,
                'choice_label' => 'name',
                'choice_value' => 'id'
            ])
            ->add('address', AddressType::class, ['mapped' => false])
            ->add('contact', ClientContactType::class, ['mapped' => false])
            ->add('consultants', EntityType::class, [
                'class' => User::class,
                'choice_label' => 'lastname',
                'choice_value' => 'id',
                'multiple' => true
            ])
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CRMBundle\Entity\Project'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'crmbundle_project';
    }


}
