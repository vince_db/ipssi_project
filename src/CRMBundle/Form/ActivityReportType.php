<?php

namespace CRMBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ActivityReportType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('startAt', DateTimeType::class, array(
                // render as a single text box
                'widget' => 'single_text',
            ))
            ->add('endAt', DateTimeType::class
        , array(
            // render as a single text box
            'widget' => 'single_text',
            ))
            ->add('numberAccidentsWithStoppage')
            ->add('numberAccidentsWithoutStoppage')
            ->add('numberAccidentsOnWay')
            ->add('numberAlmostAccidents')
            ->add('numberMedicalLeaves')
            ->add('consultantStatisfaction')
            ->add('clientSatisfaction')
            ->add('possibleImprovements')
            ->add('remainingTasks')
            ->add('clientComments')
            ->add('consultantComments')
            ->add('project')
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CRMBundle\Entity\ActivityReport'
        ));
    }
}
