<?php

namespace CRMBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class CRMBundle
 * @package CRMBundle
 * @todo Ajout validations dans entités
 * @todo Déplacer les vues dans les bons sous-dossiers et reprendre les chemins dans les controllers
 * @todo Tester les formulaires de CRUD
 * @todo Bootstrap/Présentation
 * @todo Voters pour ACL
 */
class CRMBundle extends Bundle
{
}
