<?php

namespace CRMBundle\Controller;

use CRMBundle\Entity\ClientContact;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Clientcontact controller.
 *
 * @Route("/crm/client/contact")
 */
class ClientContactController extends Controller
{
    /**
     * Lists all clientContact entities.
     *
     * @Route("/", name="clientcontact_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $clientContacts = $em->getRepository('CRMBundle:ClientContact')->findAll();

        return $this->render('crm/client/contact/index.html.twig', array(
            'zone'=>'hr',
            'title' => 'CRM : Contact Client : Liste des contacts',
            'clientContacts' => $clientContacts,
        ));
    }

    /**
     * Creates a new clientContact entity.
     *
     * @Route("/new", name="clientcontact_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $clientContact = new ClientContact();
        $form = $this->createForm('CRMBundle\Form\ClientContactType');
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $clientContact = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($clientContact);
            $em->flush($clientContact);

            return $this->redirectToRoute('clientcontact_show', array('id' => $clientContact->getId()));
        }

        return $this->render('crm/client/contact/new.html.twig', array(
            'zone'=>'hr',
            'title' => 'CRM : Contact Client : Nouveau contact',
            'clientContact' => $clientContact,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a clientContact entity.
     *
     * @Route("/{id}", name="clientcontact_show")
     * @Method("GET")
     */
    public function showAction(ClientContact $clientContact)
    {
        $deleteForm = $this->createDeleteForm($clientContact);

        return $this->render('crm/client/contact/show.html.twig', array(
            'zone'=>'hr',
            'clientContact' => $clientContact,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing clientContact entity.
     *
     * @Route("/{id}/edit", name="clientcontact_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, ClientContact $clientContact)
    {
        $deleteForm = $this->createDeleteForm($clientContact);
        $editForm = $this->createForm('CRMBundle\Form\ClientContactType', $clientContact);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('clientcontact_edit', array('id' => $clientContact->getId()));
        }

        return $this->render('crm/client/contact/edit.html.twig', array(
            'zone'=>'hr',
            'clientContact' => $clientContact,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a clientContact entity.
     *
     * @Route("/{id}", name="clientcontact_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, ClientContact $clientContact)
    {
        $form = $this->createDeleteForm($clientContact);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($clientContact);
            $em->flush($clientContact);
        }

        return $this->redirectToRoute('clientcontact_index');
    }

    /**
     * Creates a form to delete a clientContact entity.
     *
     * @param ClientContact $clientContact The clientContact entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(ClientContact $clientContact)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('clientcontact_delete', array('id' => $clientContact->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
