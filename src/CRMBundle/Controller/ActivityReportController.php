<?php

namespace CRMBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use CRMBundle\Entity\ActivityReport;
use CRMBundle\Form\ActivityReportType;

/**
 * ActivityReport controller.
 *
 * @Route("crm/activity-report")
 */
class ActivityReportController extends Controller
{
    /**
     * Lists all ActivityReport entities.
     *
     * @Route("/", name="activityreport_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $activityReports = $em->getRepository('CRMBundle:ActivityReport')->findAll();

        return $this->render('crm/activity-report/index.html.twig', array(
            'zone'=>'hr',
            'activityReports' => $activityReports,
        ));
    }

    /**
     * Creates a new ActivityReport entity.
     *
     * @Route("/new", name="activityreport_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $activityReport = new ActivityReport();
        $form = $this->createForm('CRMBundle\Form\ActivityReportType', $activityReport);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($activityReport);
            $em->flush();

            return $this->redirectToRoute('activityreport_show', array('id' => $activityReport->getId()));
        }

        return $this->render('crm/activity-report/new.html.twig', array(
            'zone'=>'hr',
            'activityReport' => $activityReport,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a ActivityReport entity.
     *
     * @Route("/{id}", name="activityreport_show")
     * @Method("GET")
     */
    public function showAction(ActivityReport $activityReport)
    {
        $deleteForm = $this->createDeleteForm($activityReport);

        return $this->render('crm/activity-report/show.html.twig', array(
            'zone'=>'hr',
            'activityReport' => $activityReport,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing ActivityReport entity.
     *
     * @Route("/{id}/edit", name="activityreport_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, ActivityReport $activityReport)
    {
        $deleteForm = $this->createDeleteForm($activityReport);
        $editForm = $this->createForm('CRMBundle\Form\ActivityReportType', $activityReport);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($activityReport);
            $em->flush();

            return $this->redirectToRoute('activityreport_edit', array('id' => $activityReport->getId()));
        }

        return $this->render('crm/activity-report/edit.html.twig', array(
            'zone'=>'hr',
            'activityReport' => $activityReport,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a ActivityReport entity.
     *
     * @Route("/{id}", name="activityreport_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, ActivityReport $activityReport)
    {
        $form = $this->createDeleteForm($activityReport);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($activityReport);
            $em->flush();
        }

        return $this->redirectToRoute('activityreport_index');
    }

    /**
     * Creates a form to delete a ActivityReport entity.
     *
     * @param ActivityReport $activityReport The ActivityReport entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(ActivityReport $activityReport)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('activityreport_delete', array('id' => $activityReport->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
