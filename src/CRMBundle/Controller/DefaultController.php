<?php

namespace CRMBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/crm")
     */
    public function indexAction()
    {
        return $this->render('crm/index.html.twig');
    }
}
