<?php

namespace HRSourcingBundle\Form\DataTransformer;

use Doctrine\Common\Persistence\ObjectManager;
use HRSourcingBundle\Entity\Resume;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\HttpFoundation\File\File;

/**
 * Class ResumeTransformer
 * @package HRSourcingBundle\Form\DataTransformer
 */
class ResumeTransformer implements DataTransformerInterface
{
    /**
     * @var
     */
    private $uploadDir;

    /**
     * ResumeTransformer constructor.
     * @param $uploadDir
     */
    public function __construct($uploadDir)
    {
        $this->uploadDir = $uploadDir;
    }

    /**
     * @param mixed $value
     * @return File
     */
    public function transform($value)
    {
        if(!is_null($value)) {
            return new File($this->uploadDir . '/' . $value->getFile());
        }
        return $value;
    }

    /**
     * @param mixed $value
     * @return string
     */
    public function reverseTransform($value)
    {
        return $value;
    }

}