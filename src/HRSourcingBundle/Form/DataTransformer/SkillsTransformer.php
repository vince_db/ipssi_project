<?php

namespace HRSourcingBundle\Form\DataTransformer;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\DataTransformerInterface;

/**
 * Class SkillsTransformer
 * @package HRS
 */
class SkillsTransformer implements DataTransformerInterface
{
    /**
     * @param mixed $value
     * @return mixed
     */
    public function transform($value)
    {
        $value = (empty($value)) ? null : implode(',', $value);
        return $value;
    }

    /**
     * @param mixed $value
     * @return string
     */
    public function reverseTransform($value)
    {
        $value = (empty($value)) ? [] : array_filter(explode(',', $value));
        return $value;
    }

}