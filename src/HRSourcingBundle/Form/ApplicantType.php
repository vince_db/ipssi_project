<?php

namespace HRSourcingBundle\Form;

use HRSourcingBundle\Form\DataTransformer\ResumeTransformer;
use HRSourcingBundle\Form\DataTransformer\SkillsTransformer;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ApplicantType extends AbstractType
{
    /**
     * @var
     */
    private $uploadDir;

    /**
     * ApplicantType constructor.
     * @param $uploadDir
     */
    public function __construct($uploadDir)
    {
        $this->uploadDir = $uploadDir;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('user', EntityType::class, [
                'class' => 'UserBundle\Entity\User',
                'choice_label' => 'lastname',
                'choice_value' => 'id'
            ])
            ->add('resume', FileType::class, ['label' => 'CV', 'required' => false])
            ->add('skills')
        ;
        $builder->get('resume')->addModelTransformer(new ResumeTransformer($this->uploadDir));
        $builder->get('skills')->addModelTransformer(new SkillsTransformer());
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'HRSourcingBundle\Entity\Applicant'
        ));
    }
}
