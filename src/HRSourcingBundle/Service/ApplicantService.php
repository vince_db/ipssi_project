<?php

namespace HRSourcingBundle\Service;

use CMSBundle\Entity\Article;
use Doctrine\Common\Persistence\ObjectManager;
use HRSourcingBundle\Entity\Applicant;
use HRSourcingBundle\Entity\Resume;

/**
 * Class ApplicantService
 * @package HRSourcingBundle\Service
 */
class ApplicantService
{
    /**
     * @var ObjectManager
     */
    private $manager;
    /**
     * @var ResumeService
     */
    private $resumeService;

    /**
     * ArticleService constructor.
     */
    public function __construct(ObjectManager $manager, ResumeService $resumeService)
    {
        $this->manager = $manager;
        $this->resumeService = $resumeService;
    }

    /**
     * Persist applicant
     * @param Applicant $applicant
     */
    public function save(Applicant $applicant)
    {
        if(!is_null($applicant->getResume())) {
            $resume = $this->resumeService->save($applicant->getResume());
            $applicant->setResume($resume);
        }

        $this->manager->persist($applicant);
        $this->manager->flush();
    }

    /**
     * Delete both applicant and resume
     * @param Applicant $applicant
     */
    public function delete(Applicant $applicant)
    {
        if(!is_null($applicant->getResume())) {
            $this->resumeService->delete($applicant->getResume());
        }

        $this->manager->remove($applicant);
        $this->manager->flush();
    }
}