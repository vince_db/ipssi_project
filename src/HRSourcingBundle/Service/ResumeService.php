<?php

namespace HRSourcingBundle\Service;

use Doctrine\Common\Persistence\ObjectManager;
use HRSourcingBundle\Entity\Resume;
use Symfony\Component\HttpFoundation\File\File;

/**
 * Class ApplicantService
 * @package HRSourcingBundle\Service
 */
class ResumeService
{
    /**
     * @var ObjectManager
     */
    private $manager;

    /**
     * @var
     */
    private $resumeDirectory;

    /**
     * ResumeService constructor.
     * @param $resumeDirectory
     */
    public function __construct(ObjectManager $manager, $resumeDirectory)
    {
        $this->manager = $manager;
        $this->resumeDirectory = $resumeDirectory;
    }

    /**
     * Save the resume file into the FS and return the file's name
     *
     * @param File $file
     * @return Resume $resume
     */
    public function save(File $file)
    {
        $fileName = md5(uniqid()) . '.' . $file->guessExtension();
        $file->move($this->resumeDirectory, $fileName);

        $resume = new Resume();
        $resume->setFile($fileName);

        exec("java -jar /opt/tika-app-1.14.jar --text --encoding=UTF8 $this->resumeDirectory/$fileName", $content);
        $resume->setContent(implode(PHP_EOL, $content));

        return $resume;
    }

    /**
     * Delete the resume + its associated file
     * @param Resume $resume
     */
    public function delete(Resume $resume)
    {
        unlink($this->getFile($resume));
        $this->manager->remove($resume);
    }

    /**
     * Returns a downloadable resume
     *
     * @param Resume $resume
     * @return File
     */
    public function getFile(Resume $resume)
    {
        return new File($this->resumeDirectory . '/' . $resume->getFile());
    }

}