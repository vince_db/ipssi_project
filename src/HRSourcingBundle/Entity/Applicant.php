<?php

namespace HRSourcingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Applicant
 *
 * @ORM\Table(name="applicant")
 * @ORM\Entity(repositoryClass="HRSourcingBundle\Repository\ApplicantRepository")
 */
class Applicant
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\OneToOne(targetEntity="UserBundle\Entity\User")
     */
    private $user;

    /**
     * @ORM\OneToOne(targetEntity="HRSourcingBundle\Entity\Resume", cascade={"persist"})
     * @ORM\JoinColumn(name="resume_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $resume;

    /**
     * @ORM\OneToMany(targetEntity="HRSourcingBundle\Entity\Application", mappedBy="applicant")
     */
    private $applications;

    /**
     * @var array
     *
     * @ORM\Column(name="skills", type="simple_array", nullable=true)
     */
    private $skills;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param string $user
     *
     * @return Applicant
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return string
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set resume
     *
     * @param string $resume
     *
     * @return Applicant
     */
    public function setResume($resume)
    {
        $this->resume = $resume;

        return $this;
    }

    /**
     * Get resume
     *
     * @return string
     */
    public function getResume()
    {
        return $this->resume;
    }

    /**
     * Set applications
     *
     * @param string $applications
     *
     * @return Applicant
     */
    public function setApplications($applications)
    {
        $this->applications = $applications;

        return $this;
    }

    /**
     * Get applications
     *
     * @return string
     */
    public function getApplications()
    {
        return $this->applications;
    }

    /**
     * Set skills
     *
     * @param array $skills
     *
     * @return Applicant
     */
    public function setSkills($skills)
    {
        $this->skills = $skills;

        return $this;
    }

    /**
     * Get skills
     *
     * @return array
     */
    public function getSkills()
    {
        return $this->skills;
    }
}

