<?php

namespace HRSourcingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Application
 *
 * @ORM\Table(name="application")
 * @ORM\Entity(repositoryClass="HRSourcingBundle\Repository\ApplicationRepository")
 */
class Application
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="HRSourcingBundle\Entity\Applicant", inversedBy="applications")
     */
    private $applicant;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="HRSourcingBundle\Entity\Position", inversedBy="applications")
     * @ORM\Column(name="position_id", nullable=true)
     */
    private $position;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set applicant
     *
     * @param string $applicant
     *
     * @return Application
     */
    public function setApplicant($applicant)
    {
        $this->applicant = $applicant;

        return $this;
    }

    /**
     * Get applicant
     *
     * @return string
     */
    public function getApplicant()
    {
        return $this->applicant;
    }

    /**
     * Set position
     *
     * @param string $position
     *
     * @return Application
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return string
     */
    public function getPosition()
    {
        return $this->position;
    }
}

