<?php

namespace HRSourcingBundle\Entity;

use AppBundle\Traits\TimestampableTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * Position
 *
 * @ORM\Table(name="position")
 * @ORM\Entity(repositoryClass="HRSourcingBundle\Repository\PositionRepository")
 */
class Position
{
    use TimestampableTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="skillLevel", type="string", length=30)
     */
    private $skillLevel;

    /**
     * @var int
     *
     * @ORM\Column(name="indicativeAnnualSalary", type="integer", nullable=true)
     */
    private $indicativeAnnualSalary;

    /**
     * @var int
     *
     * @ORM\Column(name="duration", type="integer", nullable=true)
     */
    private $duration;

    /**
     * @ORM\OneToMany(targetEntity="HRSourcingBundle\Entity\Application", mappedBy="position")
     */
    private $applications;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Position
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Position
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set skillLevel
     *
     * @param string $skillLevel
     *
     * @return Position
     */
    public function setSkillLevel($skillLevel)
    {
        $this->skillLevel = $skillLevel;

        return $this;
    }

    /**
     * Get skillLevel
     *
     * @return string
     */
    public function getSkillLevel()
    {
        return $this->skillLevel;
    }

    /**
     * Set indicativeAnnualSalary
     *
     * @param integer $indicativeAnnualSalary
     *
     * @return Position
     */
    public function setIndicativeAnnualSalary($indicativeAnnualSalary)
    {
        $this->indicativeAnnualSalary = $indicativeAnnualSalary;

        return $this;
    }

    /**
     * Get indicativeAnnualSalary
     *
     * @return int
     */
    public function getIndicativeAnnualSalary()
    {
        return $this->indicativeAnnualSalary;
    }

    /**
     * Set duration
     *
     * @param integer $duration
     *
     * @return Position
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;

        return $this;
    }

    /**
     * Get duration
     *
     * @return int
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * Get Applications
     *
     * @return mixed
     */
    public function getApplications()
    {
        return $this->applications;
    }

    /**
     * Set Applications
     *
     * @param mixed $applications
     *
     * @return Position
     */
    public function setApplications($applications)
    {
        $this->applications = $applications;
        return $this;
    }
}

