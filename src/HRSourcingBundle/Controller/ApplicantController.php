<?php

namespace HRSourcingBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use HRSourcingBundle\Entity\Applicant;
use HRSourcingBundle\Form\ApplicantType;

/**
 * Applicant controller.
 *
 * @Route("/hr/sourcing/applicant")
 */
class ApplicantController extends Controller
{
    /**
     * Lists all Applicant entities.
     *
     * @Route("/", name="applicant_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $applicants = $em->getRepository('HRSourcingBundle:Applicant')->findAll();

        return $this->render('hr/sourcing/applicant/index.html.twig', array(
            'zone'=>'hr',
            'applicants' => $applicants,
        ));
    }

    /**
     * Creates a new Applicant entity.
     *
     * @Route("/new", name="applicant_new")
     * @Method({"GET", "POST"})
     *
     * @todo Récuperer l'utilisateur courant sans passer par le formulaire
     * @todo Un utilisateur n'a qu'un applicant et qu'un CV
     */
    public function newAction(Request $request)
    {
        $applicant = new Applicant();
        $form = $this->createForm('HRSourcingBundle\Form\ApplicantType', $applicant);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->container->get('applicant.service')->save($applicant);
            return $this->redirectToRoute('applicant_show', array('id' => $applicant->getId()));
        }

        return $this->render('hr/sourcing/applicant/new.html.twig', array(
            'zone'=>'hr',
            'applicant' => $applicant,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Applicant entity.
     *
     * @Route("/{id}", name="applicant_show")
     * @Method("GET")
     */
    public function showAction(Applicant $applicant)
    {
        $deleteForm = $this->createDeleteForm($applicant);

        return $this->render('hr/sourcing/applicant/show.html.twig', array(
            'zone'=>'hr',
            'applicant' => $applicant,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Applicant entity.
     *
     * @Route("/{id}/edit", name="applicant_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Applicant $applicant)
    {
        $deleteForm = $this->createDeleteForm($applicant);
        $editForm = $this->createForm('HRSourcingBundle\Form\ApplicantType', $applicant);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->container->get('applicant.service')->save($applicant);
            return $this->redirectToRoute('applicant_edit', array('id' => $applicant->getId()));
        }

        return $this->render('hr/sourcing/applicant/edit.html.twig', array(
            'zone'=>'hr',
            'applicant' => $applicant,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Applicant entity.
     *
     * @Route("/{id}", name="applicant_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Applicant $applicant)
    {
        $form = $this->createDeleteForm($applicant);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->container->get('applicant.service')->delete($applicant);
        }

        return $this->redirectToRoute('applicant_index');
    }

    /**
     * Creates a form to delete a Applicant entity.
     *
     * @param Applicant $applicant The Applicant entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Applicant $applicant)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('applicant_delete', array('id' => $applicant->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
