<?php

namespace HRSourcingBundle\Controller;

use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use HRSourcingBundle\Entity\Resume;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

/**
 * Resume controller.
 *
 * @Route("/hr/sourcing/resume")
 */
class ResumeController extends Controller
{
    /**
     * Returns a downloadable resume
     *
     * @param Resume $resume
     * @Route("/download/{id}", name="resume_download")
     * @Method("GET")
     * @return BinaryFileResponse
     */
    public function downloadAction(Resume $resume)
    {
        $file = $this->container->get('resume.service')->getFile($resume);

        $response = new BinaryFileResponse($file);
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT);

        return $response;
    }

}
