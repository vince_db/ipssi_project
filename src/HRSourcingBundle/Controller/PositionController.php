<?php

namespace HRSourcingBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use HRSourcingBundle\Entity\Position;
use HRSourcingBundle\Form\PositionType;

/**
 * Position controller.
 *
 * @Route("/hr/sourcing/position")
 */
class PositionController extends Controller
{
    /**
     * Lists all Position entities.
     *
     * @Route("/", name="position_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $positions = $em->getRepository('HRSourcingBundle:Position')->findAll();

        return $this->render('hr/sourcing/position/index.html.twig', array(
            'zone'=>'hr',
            'positions' => $positions,
        ));
    }

    /**
     * Creates a new Position entity.
     *
     * @Route("/new", name="position_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $position = new Position();
        $form = $this->createForm('HRSourcingBundle\Form\PositionType', $position);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($position);
            $em->flush();

            return $this->redirectToRoute('position_show', array('id' => $position->getId()));
        }

        return $this->render('hr/sourcing/position/new.html.twig', array(
            'zone'=>'hr',
            'position' => $position,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Position entity.
     *
     * @Route("/{id}", name="position_show")
     * @Method("GET")
     */
    public function showAction(Position $position)
    {
        $deleteForm = $this->createDeleteForm($position);

        return $this->render('hr/sourcing/position/show.html.twig', array(
            'zone'=>'hr',
            'position' => $position,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Position entity.
     *
     * @Route("/{id}/edit", name="position_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Position $position)
    {
        $deleteForm = $this->createDeleteForm($position);
        $editForm = $this->createForm('HRSourcingBundle\Form\PositionType', $position);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($position);
            $em->flush();

            return $this->redirectToRoute('position_edit', array('id' => $position->getId()));
        }

        return $this->render('hr/sourcing/position/edit.html.twig', array(
            'position' => $position,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Position entity.
     *
     * @Route("/{id}", name="position_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Position $position)
    {
        $form = $this->createDeleteForm($position);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($position);
            $em->flush();
        }

        return $this->redirectToRoute('position_index');
    }

    /**
     * Creates a form to delete a Position entity.
     *
     * @param Position $position The Position entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Position $position)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('position_delete', array('id' => $position->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
