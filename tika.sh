#!/usr/bin/env bash
# Apache TIKA Installation
# Exec in SUDO
apt install default-jre -y
wget http://apache.mirrors.ovh.net/ftp.apache.org/dist/tika/tika-app-1.14.jar
mv tika-app-1.14.jar /opt
chmod 664 /opt/tika-app-1.14.jar