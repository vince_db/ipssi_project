# ERP IPSSI
## Installation du projet
```
git clone git@bitbucket.org:vince_db/ipssi_project.git
cd ipssi_project
git remote add upstream git@bitbucket.org:vince_db/ipssi_project.git
git checkout develop
composer install
bower install
vagrant up
```
Une fois la vagrant up, s'y connecter 
```
vagrant ssh
cd /home/vagrant/ipssi-project
```
### Créer la base de données
```
php bin/console doctrine:database:create
php bin/console doctrine:schema:update  --force
```
### Seeder la base de données
```
php bin/console hautelook_alice:fixtures:load 
```
Le projet est maintenant disponible sur [192.168.34.10](http://192.168.34.10/app_dev.php). 

Pour [se connecter en admin](http://192.168.34.10/user/login), utiliser les identifiants admin@ourcompany'com / admin.

## Bundles et entités
Par convention les noms des entités, variables ... sont en anglais. 
Tout sera crée par ```php bin/console```. Les parametres des bundles et les entités **toujours en en annotations**. 
### AppBundle
Lié à quasiment tous les autres bundles, il s'occupe des affichages du site public et regroupe des entités transverses à 
d'autres bundles.
* Menu
* Address
* Form
### UserBundle
* User
* Agency
* Role
### CMSBundle
* Page
* Category
* Article
### HRManagementBundle
* Contract
* Employee
* Expense
* ExpenseValidation
* HolidayRequest
* HolidayApproval
### HRSourcingBundle
* Position
* Skill
* Applicant
* Application
### CRMBundle
* Client
* Project
* ActivityReport

## Workflow
Chacun travaille sur son bundle. Chaque bundle est crée dans une branche issue de la develop. C'est cette branche qui fera l'objet d'une pull request sur upstream develop une fois le developpement terminé. 
### Mise à jour de votre origin sur l'upstream
Uniquement pour la branche develop qui doit être un mirroir de l'upstream
```
git checkout develop
git pull --rebase upstream develop
```
Si vous souhaitez récuperer les dernières modifications de la develop dans MyBundleBranch
```
git checkout develop
git rebase develop MyBundleBranch
```

## Configuration de PHPstorm
Pour développer efficacement, installer les plugins suivants :
* Symfony Plugin
* PHP Annotations
* Git integration

## Solutions temporaires
### Problème app_dev.php/ = Access denied
Aller dans /etc/nginx/sites-enables et modifier le fichier homestead.app. Changer ```fastcgi_split_path_info ^(.+\php)(/.+)$;``` par ```fastcgi_split_path_info ^(.+\php)(/.*)$;```

## Frontend 
### Templates bootstrap
[Site public](https://startbootstrap.com/template-overviews/modern-business/)

[Intranet](https://startbootstrap.com/template-overviews/sb-admin/)

Ne pas utiliser les icônes fournis dans ces templates, les remplacer par des [FontAwesome](http://fontawesome.io/icons/)

### Bibliothèques JS
[Chosen](https://harvesthq.github.io/chosen/) pour les liste de sélections
[Datatables](https://www.datatables.net/) pour les tables
[Pick-a-Date](http://amsul.ca/pickadate.js/) pour les dates (et les heures si besoin)
